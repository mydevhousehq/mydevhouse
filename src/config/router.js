import Vue from "vue";
import VueRouter from "vue-router";

Vue.use(VueRouter);

import Home from "@/components/Home";
import About from "@/components/About";
import Services from "@/components/Services";
import Portfolio from "@/components/Portfolio";
import Testimonials from "@/components/Testimonials";
import Contact from "@/components/Contact";
import Thanks from "@/components/Thanks";
import Error from "@/components/404";

import Process from "@/components/about/Process";
import Team from "@/components/about/Team";

import WebDevelopment from "@/components/services/WebDevelopment";
import SystemsDevelopment from "@/components/services/SystemsDevelopment";
import MobileDevelopment from "@/components/services/MobileDevelopment";

const routes = [
  {
    path: '/',
    name: 'home',
    component: Home,
    meta: {
      auth: false,
      title: "MyDevHouse"
    }
  },
  {
    path: '/about',
    name: 'about',
    component: About,
    meta: {
      auth: false,
      title: "About Us"
    },
    children: [
      {
        path: '/about/our-team',
        name: 'our-team',
        meta: {
          auth: false,
          title: "Our Team"
        },
        components: {
          about: Team
        }
      },
      {
        path: '/about/our-process',
        name: 'our-process',
        meta: {
          auth: false,
          title: "Our Process"
        },
        components: {
          about: Process
        }
      },
    ]
  },
  {
    path: '/services',
    name: 'services',
    component: Services,
    meta: {
      auth: false,
      title: "Our Services"
    },
    children: [
      {
        path: '/services/web-development',
        name: 'web-development',
        meta: {
          auth: false,
          title: "Web Development"
        },
        components: {
          services: WebDevelopment
        }
      },
      {
        path: "/services/systems-development",
        name: "systems-development",
        meta: {
          auth: false,
          title: "Systems Development"
        },
        components: {
          services: SystemsDevelopment
        }
      },
      {
        path: "/services/mobile-development",
        name: "mobile-development",
        meta: {
          auth: false,
          title: "Mobile Development"
        },
        components: {
          services: MobileDevelopment
        }
      },
    ]
  },
  {
    path: '/portfolio',
    name: 'portfolio',
    component: Portfolio,
    meta: {
      auth: false,
      title: "Portfolio"
    }
  },
  {
    path: '/testimonials',
    name: 'testimonials',
    component: Testimonials,
    meta: {
      auth: false,
      title: "Testimonials"
    }
  },
  {
    path: '/contact',
    name: 'contact',
    component: Contact,
    meta: {
      auth: false,
      title: "Contact Us"
    }
  },
  {
    path: '/thanks',
    name: 'thanks',
    component: Thanks,
    meta: {
      auth: false,
      title: "Thank You"
    }
  },
  { 
    path: '/404', 
    name: 'error',
    alias: '*', 
    component: Error,
    meta: {
      auth: false,
      title: "Page not found"
    } 
  },
];

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
});

export default router