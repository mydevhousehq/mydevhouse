import Vue from "vue";
import App from "./App.vue";

// Bootstrap Vue
import BootstrapVue from "bootstrap-vue";
Vue.use(BootstrapVue);

// VueRouter
import router from "@/config/router";

// Axios Configuration
import axios from "axios";

Vue.prototype.$axios = axios.create({
  baseURL:
    process.env.NODE_ENV === "production"
      ? "https://mydevhouse.dev/wp-json"
      : "http://mydevhouse.test/wp-json"
});

// Work Sans Font
require("typeface-work-sans");

// Main SCSS File
import "@/assets/scss/resume.scss";

// Vue Overlay CSS
import "vue-loading-overlay/dist/vue-loading.css";

// Filters
Vue.filter("replacedash", function(value) {
  if (!value) {
    return "";
  } else {
    value = value.replace(/-/g, " ");
    return value;
  }
});

Vue.filter("removeHtml", function(value) {
  if (!value) {
    return "";
  } else {
    value = value.replace(/(<([^>]+)>)/gi, "");
    return value;
  }
});

Vue.config.productionTip = false;

new Vue({
  router,
  render: h => h(App)
}).$mount("#app");
