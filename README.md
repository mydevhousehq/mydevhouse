# Introduction

This is a SPA(Single Page Application) for MyDevHouse that gets most of its data from WordPress through REST API. It will eventually be migrated to JAMStack like Gridsome/VuePress in the future.

## Building from Source

1. Install [Git](https://git-scm.com/).
2. Clone or download the repository to your local machine.
3. Install [Node](https://nodejs.org/en/).
4. Install [Yarn](https://yarnpkg.org).
5. Install Vue CLI globally, `yarn global add @vue/cli` or `npm i -g @vue/cli`.
6. Run `yarn install` to install dependencies through terminal/CLI program.
7. Run `yarn run serve` through your favorite CLI program.

 **Note:** I suggest using package manager to install Git, Node and Yarn. You can use [Homebrew](httsp://brew.sh) if you're on a Mac or Linux/WSL, [Scoop](https://scoop.sh) or [Chocolatey](https://chocolatey.org/) if you're on Windows.

## Features

1. Built with Bootstrap Vue and Feather Icons.
2. No jQuery dependency.
3. Pull data from WordPress with WP REST API/ACF to REST.

## Todos

- [ ] Bug fixes and cleanup.
- [x] Add transition effect to sidebar and clicked items(Profile, Portfolio, Services).
- [x] Add loading indicator for content pulled from WordPress.
- [x] Better handling of fixed elements and scrollable content.
- [x] Fix issues with responsiveness.
- [x] Better font scaling.
- [x] Navigation menu animation.
- [ ] Setup formspree.io/Netlify forms to catch emails from contact forms.
- [ ] Migrate to Gridsome / VuePress.

## Credits

* [StartBootstrap Resume](https://github.com/BlackrockDigital/startbootstrap-resume)
* [Bootstrap Vue](https://bootstrap-vue.js.org/)
* [Vue Feather Icons](https://github.com/egoist/vue-feather-icons)